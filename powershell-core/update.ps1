#[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
import-module au
. $PSScriptRoot\..\_scripts\all.ps1

function global:au_SearchReplace {
   @{
        ".\tools\chocolateyInstall.ps1" = @{
            "(?i)(^\s*[$]Version\s*=\s*)('.*')"= "`$1'$($Latest.Version)'"
            "(?i)(^\s*[$]packageName\s*=\s*)('.*')"= "`$1'$($Latest.PackageName)'"
            "(?i)(^\s*[$]fileType\s*=\s*)('.*')"   = "`$1'$($Latest.FileType)'"
            "(?i)(^\s*url\s*=\s*)('.*')"          = "`$1'$($Latest.URL32)'"
            "(?i)(^\s*checksum\s*=\s*)('.*')"     = "`$1'$($Latest.Checksum32)'"
            "(?i)(^\s*url64\s*=\s*)('.*')"        = "`$1'$($Latest.URL64)'"
            "(?i)(^\s*checksum64\s*=\s*)('.*')"   = "`$1'$($Latest.Checksum64)'"           
        }

       ".\powershell-core.nuspec" = @{
           "(\<releaseNotes\>).*?(\</releaseNotes\>)" = "`${1}$($Latest.ReleaseNotes)`$2"
       }        
    }
}

function global:au_AfterUpdate  { Set-DescriptionFromReadme -SkipFirst 2 }

function global:au_GetLatest {
    $token = $env:github_api_key
    $script:headers = @{
        'User-Agent' = 'DarwinJS'
    }
    if ($token) {
        $script:headers['Authorization'] = ("token {0}" -f $token)
    } else {
        Write-Warning "No auth token"
    }

    $releases = Invoke-RestMethod -Method Get -Uri 'https://api.github.com/repos/PowerShell/PowerShell/releases' -Headers $headers

    $latest = @{
        Streams = [ordered] @{
        }
    }

    # Example version "v7.3.0-preview.8"
    $releases | Group-Object -Property { ($_.tag_name.Substring(1).Split(".", 3) | Select-Object -First 2) -join "." } | ForEach-Object { 

        # First result in group is most recent
        $release = $_.Group[0]

        $url = $release.assets | Where-Object { $_.name.EndsWith('win-x64.msi')} | Select-Object -ExpandProperty browser_download_url -First 1
        $url32 = $release.assets | Where-Object { $_.name.EndsWith('win-x86.msi')} | Select-Object -ExpandProperty browser_download_url -First 1

        # Convert SemVer2 to SemVer1, and pad with zero so that 09 comes before 10 etc
        $version = $release.tag_name.Substring(1) -replace "-(\w+).", '-$1' -replace "preview(\d)$", 'preview0$1' -replace ' ', ''
    
        $latest.Streams.Add($_.Name, @{
            URL64        = $url;
            URL32        = $url32;
            Version      = $version
            ReleaseNotes = $release.html_url
            PackageName = 'powershell-core';
        })
    }

    return $latest
}

update
